# 3. Tipos de datos

Ya explicamos que al crear una tabla debemos resolver qué campos (columnas) tendrá y que tipo de datos almacenará cada uno de ellos, es decir, su estructura.

El tipo de dato especifica el tipo de información que puede guardar un campo: caracteres, números, etc.

Estos son algunos tipos de datos básicos de Oracle (posteriormente veremos otros y con más detalle):

* varchar2: se emplea para almacenar cadenas de caracteres. Una cadena es una secuencia de caracteres. Se coloca entre comillas simples; ejemplo: 'Hola', 'Juan Perez', 'Colon 123'. Este tipo de dato definen una cadena de longitud variable en la cual determinamos el máximo de caracteres entre paréntesis. Puede guardar hasta xxx caracteres. Por ejemplo, para almacenar cadenas de hasta 30 caracteres, definimos un campo de tipo varchar2 (30), es decir, entre paréntesis, junto al nombre del campo colocamos la longitud.

Si intentamos almacenar una cadena de caracteres de mayor longitud que la definida, la cadena no se carga, aparece un mensaje indicando tal situación y la sentencia no se ejecuta.

Por ejemplo, si definimos un campo de tipo varchar(10) e intentamos almacenar en él la cadena 'Buenas tardes', aparece un mensaje indicando que el valor es demasiado grande para la columna.

* number(p,s): se usa para guardar valores numéricos con decimales, de 1.0 x10-120 a 9.9...(38 posiciones). Definimos campos de este tipo cuando queremos almacenar valores numéricos con los cuales luego realizaremos operaciones matemáticas, por ejemplo, cantidades, precios, etc.
  
Puede contener números enteros o decimales, positivos o negativos. El parámetro "p" indica la precisión, es decir, el número de dígitos en total (contando los decimales) que contendrá el número como máximo. El parámetro "s" especifica la escala, es decir, el máximo de dígitos decimales. Por ejemplo, un campo definido "number(5,2)" puede contener cualquier número entre 0.00 y 999.99 (positivo o negativo)

Para especificar número enteros, podemos omitir el parámetro "s" o colocar el valor 0 como parámetro "s". Se utiliza como separador el punto (.)

Si intentamos almacenar un valor mayor fuera del rango permitido al definirlo, tal valor no se carga, aparece un mensaje indicando tal situación y la sentencia no se ejecuta.

Por ejemplo, si definimos un campo de tipo number(4,2) e intentamos guardar el valor 123.45, aparece un mensaje indicando que el valor es demasiado grande para la columna. Si ingresamos un valor con más decimales que los definidos, el valor se carga pero con la cantidad de decimales permitidos, los dígitos sobrantes se omiten.

Antes de crear una tabla debemos pensar en sus campos y optar por el tipo de dato adecuado para cada uno de ellos.

Por ejemplo, si en un campo almacenaremos números telefónicos o un números de documento, usamos "varchar2", no "number" porque si bien son dígitos, con ellos no realizamos operaciones matemáticas. Si en un campo guardaremos apellidos, y suponemos que ningún apellido superará los 20 caracteres, definimos el campo "varchar2(20)". Si en un campo almacenaremos precios con dos decimales que no superarán los 999.99 pesos definimos un campo de tipo "number(5,2)", es decir, 5 dígitos en total, con 2 decimales. Si en un campo almacenaremos valores enteros de no más de 3 dígitos, definimos un campo de tipo "number(3,0)".

## Ejercicios de laboratorio

Eliminamos la tabla "libros":

```sql
 drop table libros;
```

Vamos a crear una tabla llamada "libros" para almacenar información de los libros de una librería. Necesitamos los siguientes campos:

```sh
 -titulo: cadena de caracteres de 20 de longitud,
 -autor: cadena de caracteres de 15 de longitud,
 -editorial: caracteres de 10 de longitud,
 -precio: valor numérico con 2 decimales y que no superará el valor 9999.99 y
 -cantidad: valor numérico entero que no superará el valor 999.
```

Al crear la tabla, entonces, elegimos el tipo de dato más adecuado para cada campo:

```sql
create table libros(
    titulo varchar2(20),
    autor varchar2(15),
    editorial varchar2(10),
    precio number(6,2),
    cantidad number(3,0)
);
```

Vemos la estructura de la tabla:

```sql
describe libros;
```

Aparece la siguiente información:

```sh
Name Null Type
--------------------------------------

titulo  varchar2(20)
autor  varchar2(15)
editorial varchar2(10)
precio  number(6,2)
cantidad number(3)
```

Ingresamos algunos registros:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',25.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Siglo XXI',18.8,200);
```

Note que al ingresar valores numéricos no se utilizan comillas y para el separador de decimales se usa el caracter punto (.).

Veamos los registros cargados:

```sql
 select * from libros;
```

Aparece la siguiente tabla:

```sh
TITULO   AUTOR EDITORIAL PRECIO CANTIDAD
----------------------------------------------------------------

El Aleph  Borges Emece  25,5 100
Matematica estas ahi Paenza Siglo XXI 18,8 200
```

Veamos lo que sucede si intentamos ingresar para el campo "titulo" una cadena de más de 20 caracteres:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Atlantida',10,200);
```

Aparece un mensaje de error y la sentencia no se ejecuta.

Vamos a cortar la cadena para que Oracle acepte el ingreso del registro:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais','Lewis Carroll','Atlantida',10,200);
```

Veamos los registros cargados:

```sql
select * from libros;
```

La tabla tiene ahora 3 registros.

Veamos qué sucede si intentamos ingresar para el campo "cantidad" un valor fuera de rango:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10,2000);
```

Oracle muestra un mensaje de error y la sentencia no se ejecuta, es decir, el registro no fue ingresado.

Veamos qué sucede si intentamos ingresar en el campo "precio" un valor con más decimales que los permitidos:

```sql
insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10.123,200);
```

La sentencia se ejecutó, el registro ha sido cargado. Veamos cómo se almacenó:

```sql
select * from libros;
```

Oracle omitió el último dígito decimal porque el campo sólo admitía 2 decimales.

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    titulo varchar2(20),
    autor varchar2(15),
    editorial varchar2(10),
    precio number(6,2),
    cantidad number(3,0)
);

describe libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El aleph','Borges','Emece',25.50,100);
insert into libros (titulo,autor,editorial,precio,cantidad) values ('Matematica estas ahi','Paenza','Siglo XXI',18.8,200);

select * from libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais de las maravillas','Lewis Carroll','Atlantida',10,200);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('Alicia en el pais','Lewis Carroll','Atlantida',10,200);

select * from libros;

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10,2000);

insert into libros (titulo,autor,editorial,precio,cantidad) values ('El gato con botas','Anonimo','Atlantida',10.123,200);

select * from libros;
```

La ejecución de este lote de comandos SQL genera una salida similar a:

![3](imagenes/3/1.jpg)

## Ejercicios propuestos

### Ejercicio 01

Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

```sh
 -nombre, cadena de caracteres de 20 de longitud,
 -actor, cadena de caracteres de 20 de longitud,
 -duración, valor numérico entero que no supera los 3 dígitos.
 -cantidad de copias: valor entero de un sólo dígito (no tienen más de 9 copias de cada película).
```

1. Elimine la tabla "peliculas" si ya existe.

2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo.

> Note que los campos "duracion" y "cantidad", que almacenarán valores sin decimales, fueron definidos de maneras diferentes, en el primero especificamos el valor 0 como cantidad de decimales, en el segundo no especificamos cantidad de decimales, es decir, por defecto, asume el valor 0.

3. Vea la estructura de la tabla.

4. Ingrese los siguientes registros:

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);
```

5. Muestre todos los registros (4 registros)

6. Intente ingresar una película con valor de cantidad fuera del rango permitido:

```sql
 insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10);
```

> Mensaje de error.

7. Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);
```

8. Muestre todos los registros para ver cómo se almacenó el último registro ingresado.

9. Intente ingresar un nombre de película que supere los 20 caracteres.

### Ejercicio 02

Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.

1. Elimine la tabla si existe.

2. Cree la tabla eligiendo el tipo de dato adecuado para cada campo:

```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);
```

3. Verifique que la tabla existe consultando

```sql
"all_tables"
```

4. Vea la estructura de la tabla (5 campos)

5. Ingrese algunos registros:

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);
```

6. Seleccione todos los registros (3 registros)

7. Intente ingresar un registro con el valor "masculino" en el campo "sexo".

    Un mensaje indica que el campo está definido para almacenar 1 solo caracter como máximo y está intentando ingresar 9 caracteres.

8. Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico"
Mensaje de error.

9. Elimine la tabla
