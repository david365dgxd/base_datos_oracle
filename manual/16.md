# 16. Ingresar algunos campos

Hemos aprendido a ingresar registros listando todos los campos y colocando valores para todos y cada uno de ellos luego de "values".

Si ingresamos valores para todos los campos, podemos omitir la lista de nombres de los campos.
Por ejemplo, si tenemos creada la tabla "libros" con los campos "titulo", "autor" y "editorial", podemos ingresar un registro de la siguiente manera:

```sql
insert into libros values ('Uno','Richard Bach','Planeta');
```

También es posible ingresar valores para algunos campos. Ingresamos valores solamente para los campos "titulo" y "autor":

```sql
insert into libros (titulo, autor) values ('El aleph','Borges');
```

Oracle almacenará el valor "null" en el campo "editorial", para el cual no hemos explicitado un valor.

Al ingresar registros debemos tener en cuenta:

. la lista de campos debe coincidir en cantidad y tipo de valores con la lista de valores luego de "values". Si se listan más (o menos) campos que los valores ingresados, aparece un mensaje de error y la sentencia no se ejecuta.

. si ingresamos valores para todos los campos podemos obviar la lista de campos.

. podemos omitir valores para los campos que permitan valores nulos (se guardará "null"); si omitimos el valor para un campo "not null", la sentencia no se ejecuta.

## Practica de laboratorio

Trabajamos con la tabla "libros" que almacena los datos de los libros de una librería.

Eliminamos la tabla:

```sql
drop table libros;
```

Creamos la tabla:

```sql
create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(15)
);
```

Si ingresamos valores para todos los campos, podemos omitir la lista de campos:

```sql
insert into libros values (1,'Uno','Richard Bach','Planeta');
```

Podemos ingresar valores para algunos de los campos:

```sql
insert into libros (codigo, titulo, autor) values (2,'El aleph','Borges');
```

Veamos cómo Oracle almacenó los registros:

```sql
select *from libros;
```

En el campo "editorial", para el cual no ingresamos valor, se almacenó "null".

No podemos omitir el valor para un campo declarado "not null", como el campo "codigo":

```sql
insert into libros (titulo, autor,editorial)
values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');
```

Aparece un mensaje y la inserción no se realiza.

Veamos cómo Oracle almacenó los registros:

```sql
select *from libros;
```

Ingresemos el siguiente lote de comandos en el Oracle SQL Developer:

```sql
drop table libros;

create table libros(
    codigo number(5) not null,
    titulo varchar2(40) not null,
    autor varchar2(30),
    editorial varchar2(15)
);

insert into libros
values (1,'Uno','Richard Bach','Planeta');

insert into libros (codigo, titulo, autor)
values (2,'El aleph','Borges');

select *from libros;

insert into libros (titulo, autor,editorial)
values ('Alicia en el pais de las maravillas','Lewis Carroll','Planeta');

select *from libros;
```

## Ejercicios propuestos

Primer problema:
Un banco tiene registrados las cuentas corrientes de sus clientes en una tabla llamada "cuentas".

1. Elimine la tabla "cuentas":

```sql
drop table cuentas;
```

2. Cree la tabla :

```sql
create table cuentas(
    numero number(10) not null,
    documento char(8) not null,
    nombre varchar2(30),
    saldo number(9,2)
);
```

3. Ingrese un registro con valores para todos sus campos, omitiendo la lista de campos.

4. Ingrese un registro omitiendo algún campo que admita valores nulos.

5. Verifique que en tal campo se almacenó "null"

6. Intente ingresar un registro listando 3 campos y colocando 4 valores. Un mensaje indica que hay demasiados valores.

7. Intente ingresar un registro listando 3 campos y colocando 2 valores. Un mensaje indica que no hay suficientes valores.

8. Intente ingresar un registro sin valor para un campo definido "not null".

9. Vea los registros ingresados.
